export default {
    ADD_ACCORDION_ACTION(context, accordion) {
        context.commit("ADD_ACCORDION_MUTATION", accordion);
    },
    DELETE_ACCORDION_ACTION(context, index) {
        context.commit("DELETE_ACCORDION_MUTATION", index);
    },
    RESET_ACCORDION_ACTION(context) {
        context.commit("RESET_ACCORDION_MUTATION");
    },
    SET_ACCORDION_ACTION(context, accordion, index) {
        context.commit("SET_ACCORDION_MUTATION", accordion, index);
    },
};

export const mutations = {
    ADD_ACCORDION_MUTATION(state, payload) {
        //state.accordions.push(payload)
        state.accordions = [...state.accordions, payload];
        console.log("+1");
        console.log(state.accordions);
    },
    DELETE_ACCORDION_MUTATION(state, index) {
        state.accordions.splice(index, 1);
        console.log("-1 mut");
        console.log(state.accordions);
        //let id = newAccordion.id
        let deletedAccordion = state.accordions[index];
        state.accordions = [
            ...state.accordions.filter(
                accordion => accordion && accordion.id !== deletedAccordion.id
            ),
        ];
        console.log(state.accordions);
        console.log("mut done");
    },
    RESET_ACCORDION_MUTATION(state) {
        state.accordions = [];
        console.log("dlt all mut");
        console.log(state.accordions);
    },
    SET_ACCORDION_MUTATION(state, newAccordion) {
        console.log(state.accordions);
        console.log("mut");
        //let id = newAccordion.id
        // state.accordions = [
        // 	...state.accordions.filter(accordion => accordion.id !== id),
        // 	newAccordion,
        // ]

        // find index
        // let index = state.accordions.findIndex(
        // 	accordion => accordion.id === newAccordion
        // )
        // if (index !== -1) state.accordions[index] === newAccordion
        // slice
        let index = state.accordions.findIndex(
            accordion => accordion.id === newAccordion
        );
        if (index !== -1) state.accordions.splice(index, 1, newAccordion);

        if (index !== -1) {
            let acc = state.accordions.slice(index, newAccordion);
            state.accordions = acc;
        }
        console.log(state.accordions);
    },
};

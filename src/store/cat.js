import axios from "axios";
//import router from "../router";

export default {
    namespaced: true,
    state: {
        cat: [],
    },
    getters: {},
    mutations: {
        CATS_MUTATION(state, cats) {
            state.cat = cats;
        },
    },
    actions: {
        async ADD_CAT_ACTION(context, payload) {
            console.log("cat payload", payload);
            try {
                let response = await axios({
                    method: "post",
                    url: "http://localhost:3002/auth/addCat",
                    data: {
                        cat: payload.cat,
                        // id: localStorage.getItem("userId"),
                    },
                    headers: {
                        Authorization: "Bearer " + context.rootState.user.token,
                    },
                    withCredentials: true,
                });

                console.log(response);
            } catch (error) {
                console.log("error" + error.response.data.message);

                return error.response.data.message;
            } finally {
                console.log("finally in action");
            }
        },
        async ALL_CATS_ACTION(context) {
            console.log(
                "ALL_CATS_ACTION, username:",
                localStorage.getItem("username")
            );
            try {
                let response = await axios({
                    method: "post",
                    url: "http://localhost:3002/auth/allCats",
                    data: {
                        id: localStorage.getItem("username"),
                    },
                    withCredentials: true,
                });
                let cats = response.data.cats;
                console.log("response.data.cats:", cats);
                context.commit("CATS_MUTATION", cats);
                // commit("SET_TOKEN_MUTATION", response.data.token);
                // console.log("LOGIN FUNCTION STATE user: ", state.token);
            } catch (error) {
                console.log("error" + error.response.data.message);
                // commit("SET_USER_MUTATION", null);
                // commit("SET_TOKEN_MUTATION", null);
                return error.response.data.message;
            } finally {
                console.log("finally in action");
            }
        },
    },
};

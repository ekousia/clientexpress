import Vue from "vue";
import Vuex from "vuex";
//import axios from 'axios'
//import router from '../router'
import userModule from "../store/user";
import accordionModule from "../store/accordion";
import catModule from "../store/cat";

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        user: userModule,
        accordion: accordionModule,
        cat: catModule,
    },
    state: {
        axiosRequestCompleted: false,
        loggedIn: null,
    },
    mutations: {
        SET_AXIOS_REQUEST(state, axiosRequestCompleted) {
            state.axiosRequestCompleted = axiosRequestCompleted;
        },
    },
    actions: {
        SET_AXIOS_REQUEST(context, axiosRequestCompleted) {
            context.commit("SET_AXIOS_REQUEST", axiosRequestCompleted);
        },
    },
});

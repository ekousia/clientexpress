import axios from "axios";
import router from "../router";

export default {
    namespaced: true,
    state: {
        user: localStorage.getItem("username"),
        userId: localStorage.getItem("userId"),
        token: localStorage.getItem("token"),
    },

    getters: {
        is_user_authenticated: state => {
            return !!state.token;
        },
    },
    mutations: {
        SET_USER_MUTATION(state, user) {
            console.log("SET_USER_MUTATION user: ", user);
            state.user = user;
            console.log("SET USER MUTATION STATE: ", state.user);
            if (user) {
                localStorage.setItem("username", state.user.username);
                localStorage.setItem("userId", state.user.id);
            } else {
                localStorage.removeItem("user");
                localStorage.removeItem("userId");
                state.user = null;
            }
        },
        SET_TOKEN_MUTATION(state, token) {
            console.log("333333333333", token);
            if (token) {
                console.log("inside SET_TOKEN_MUTATION", state.token);
                state.token = token;
                localStorage.setItem("token", state.token);
            } else {
                localStorage.removeItem("token");
                state.token = null;
                console.log("inside SET_TOKEN_MUTATION", state.token);
            }
        },
    },
    actions: {
        SET_USER_ACTION(context, user) {
            router.push({ path: "/login" });
            context.commit("SET_USER_MUTATION", user);
        },
        RESET_USER_ACTION(context) {
            context.commit("RESET_USER_MUTATION");
        },
        async SIGN_UP_ACTION(context, payload) {
            try {
                let response = await axios({
                    method: "post",
                    url: "http://localhost:3002/user/signup",
                    data: {
                        username: payload.username,
                        password: payload.password,
                        first_name: payload.first_name,
                        role: payload.role,
                    },
                });
                //let username = payload.username;
                let newUser = {
                    username: response.data.user.username,
                    id: response.data.user.id,
                };
                context.commit("SET_USER_MUTATION", newUser);
                context.commit("SET_TOKEN_MUTATION", response.data.token);
                // context.commit("SET_USER_MUTATION", username);
                // router.push({ path: "/accordions" });
                return response.data.message;
            } catch (error) {
                console.log("error" + error.response.data.message);
                return error.response.data.message;
            } finally {
                console.log("finally in action");
            }
        },
        async LOGIN({ commit, state }, payload) {
            console.log("payload", payload);
            try {
                let response = await axios({
                    method: "post",
                    url: "http://localhost:3002/auth/login",
                    data: {
                        username: payload.username,
                        password: payload.password,
                    },
                    withCredentials: true,
                });
                // console.log("333333", response);
                console.log(
                    "LOGIN FUNCTION RESPONSE user:",
                    response.data.user.username,
                    "token: ",
                    response.data.token
                );
                let newUser = {
                    username: response.data.user.username,
                    id: response.data.user.id,
                };
                console.log("response.data.token: ", response.data.token);
                commit("SET_USER_MUTATION", newUser);
                commit("SET_TOKEN_MUTATION", response.data.token);
                console.log("LOGIN FUNCTION STATE user: ", state.token);
            } catch (error) {
                console.log("error" + error.response.data.message);
                commit("SET_USER_MUTATION", null);
                commit("SET_TOKEN_MUTATION", null);
                return error.response.data.message;
            } finally {
                console.log("finally in action");
            }
        },
        async ME({ state }) {
            try {
                let response = await axios({
                    method: "get",
                    url: "http://localhost:3002/auth/me",
                    headers: {
                        Authorization: "Bearer " + state.token,
                    },
                    // withCredentials: true
                });
                console.log("MMMMMMME response:", response);
                //commit("SET_USER_MUTATION", response.data.user);
                // commit("SET_TOKEN_MUTATION", response.data.token);
                console.log("122222222222MMMMMMME token:", state.token);
            } catch (err) {
                console.error("error: ", err);
            }
        },
        async LOGOUT({ commit, state }) {
            try {
                let response = await axios({
                    method: "get",
                    url: "http://localhost:3002/auth/logout",
                    headers: {
                        Authorization: "Bearer " + state.token,
                    },
                });
                console.log("logout function in store: ", response.data.user);
                commit("SET_USER_MUTATION", response.data.user);
                commit("SET_TOKEN_MUTATION", response.data.token);
            } catch (error) {
                console.log("error" + error.response.data.message);
                commit("SET_USER_MUTATION", null);
                commit("SET_TOKEN_MUTATION", null);
                return error.response.data.message;
            }
        },
        async VERIFY(context, payload) {
            console.log(payload);
            try {
                let response = await axios({
                    method: "get",
                    url: "http://localhost:3002/auth/verify",
                    withCredentials: true,
                });
                console.log(response);
            } catch (error) {
                console.log("error" + error.response.data.message);
                return error.response.data.message;
            } finally {
                console.log("finally in action");
            }
        },
    },
};

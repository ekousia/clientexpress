import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "bulma/css/bulma.css";
import VueSimpleAlert from "vue-simple-alert";
// import '../src/assets/fontawesome-free-5.13.0-web/css/all.css'

Vue.config.productionTip = false;
Vue.use(VueSimpleAlert);

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount("#app");

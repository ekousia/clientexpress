import Vue from "vue";
import VueRouter from "vue-router";
import Login from "../views/Login.vue";
import LoggedIn from "../components/LoggedInMessage.vue";
import Home from "../components/Home.vue";
import SignUp from "../views/SignUp.vue";
import store from "../store";

Vue.use(VueRouter);

const routes = [
    {
        path: "/",
        name: "Home",
        component: Home,
        meta: {
            allUsers: true,
        },
    },
    {
        path: "/login",
        name: "Login",
        component: Login,
        meta: {
            guestUser: true,
        },
    },
    {
        path: "/signup",
        name: "Sign Up",
        component: SignUp,
        meta: {
            guestUser: true,
        },
    },

    {
        path: "/loggedIn",
        name: "LoggedIn",
        component: LoggedIn,
        meta: {
            authorizedUser: true,
        },
    },

    {
        path: "/accordions",
        name: "AccordionsList",
        // route level code-splitting
        // this generates a separate chunk (about.[hash].js) for this route
        // which is lazy-loaded when the route is visited.
        component: () =>
            import(
                /* webpackChunkName: "about" */ "../views/AccordionsList.vue"
            ),
        meta: {
            authorizedUser: true,
        },
    },
    {
        path: "/cats",
        name: "Cats",
        component: () =>
            import(/* webpackChunkName: "about" */ "../views/Cats.vue"),
        meta: {
            authorizedUser: true,
        },
    },
];

const router = new VueRouter({
    routes,
});

router.beforeEach((to, from, next) => {
    let isUserAuth = store.getters["user/is_user_authenticated"];
    console.log("user auth: ", isUserAuth);

    if (to.meta.guestUser) {
        if (!isUserAuth) {
            console.log(
                "this is a guest user going to a route for guest users only"
            );
            next();
        } else {
            console.log(
                "this is an authorized user going to a route for guest users only, redirected to home page"
            );
            next("/");
        }
    }
    if (to.meta.authorizedUser) {
        if (isUserAuth) {
            console.log(
                "this is an authorized user going to a route for authorized users only"
            );
            next();
        } else {
            console.log(
                "this is a gust user going to a route for authorized users only, redirected to home page"
            );
            next("/");
        }
    }
    if (to.meta.allUsers) {
        next();
    }
});

export default router;
